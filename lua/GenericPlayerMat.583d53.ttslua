#include buildingBlocks/clonableTile
#include staticData/colors

loadCallback = function(wasRestored)
  if wasRestored then initialize() end
 end

genericCounterTokenGuid = "2a345b"
genericProductionCounterTokenGuid = "7697f7"

defaultObjectState.tokenGuids = {
    coloredRim = "98d93c",
    credits = {
			stockPile = "30b55f",
			production = "42c81e",
      friendlyName = "Megacredits"
		},

		steel = {
			stockPile = "6280c6",
			production = "bf2937",
      friendlyName = "Steel"
		},

		titanium = {
			stockPile = "4d0bdc",
			production = "8c22e3",
      friendlyName = "Titanium"
		},

		plants = {
			stockPile = "538a35",
			production = "130521",
      friendlyName = "Plants"
		},

		energy = {
		  stockPile = "00b7d0",
			production = "1ca787",
      friendlyName = "Energy"
		},

		heat = {
			stockPile = "afce11",
			production = "713ae0",
      friendlyName = "Heat"
		}
}

function setCounterScripts()
	local stockPileScript = getObjectFromGUID(genericCounterTokenGuid).getLuaScript()
	local productionScript = getObjectFromGUID(genericProductionCounterTokenGuid).getLuaScript()
	for resourceName, resourceType in pairs(objectState.tokenGuids) do
    if type(resourceType) == "table" then
			getObjectFromGUID(resourceType.stockPile).interactable = false
			getObjectFromGUID(resourceType.stockPile).setLuaScript(stockPileScript)
			getObjectFromGUID(resourceType.production).setLuaScript(productionScript)
			getObjectFromGUID(resourceType.production).interactable = false
			getObjectFromGUID(resourceType.production).setName(resourceType.friendlyName)
    end
	end
end

function selfDestruct()
	for _, resType in pairs(objectState.tokenGuids) do
    if type(resType) == "table" then
      getObjectFromGUID(resType.stockPile).destruct()
  		getObjectFromGUID(resType.production).destruct()
    else
      getObjectFromGUID(resType).destruct()
    end
	end
	self.destruct()
end

function initializeCopy()
  setCounterScripts()
  initialize()
  setDescendantPlayerNames()
  self.interactable = false
  getObjectFromGUID(objectState.tokenGuids.coloredRim).interactable = false
  getObjectFromGUID(objectState.tokenGuids.coloredRim).setColorTint(stringColorToRGB(objectState.playerColor))
end

function initialize()
  self.createButton({
    click_function = 'increaseTRPlayerClick',
    label = '+',
    function_owner = self,
    position = { - 1.13, 0.2, - 0.27},
    rotation = {0, 0, 0},
    width = 100,
    height = 50,
    font_size = 80,
    color = {1, 0, 0}
  })

  self.createButton({
    click_function = 'decreaseTRPlayerClick',
    label = '-',
    function_owner = self,
    position = { - 1.13, 0.2, - 0.02},
    rotation = {0, 0, 0},
    width = 50,
    height = 50,
    font_size = 80,
    color = {0.52941176470588, 0.8078431372549, 0.92156862745098}
  })

  self.createButton({
    function_owner = self,
    click_function = "rePositionSelf",
    position = {-1.2, 0.25, 1.125},
    label = "Fix Board Pos/Rot",
    scale = {0.25,1,0.25},
    width = 1080,
    height = 300,
    font_size = 110
  })
  self.createButton({
    function_owner = self,
    click_function = "toggleAntiLag",
    position = {-0.6, 0.25, 1.125},
    label = "Toggle Anti Lag Board",
    scale = {0.25,1,0.25},
    tooltip = "Toggle anti lag board.\nLag may be caused by too many cards in all players' hands.\nTo reduce lag place cards from your hand onto the anti lag board.\n  Remark: Cards may stack on the anti-lag board, so take care.",
    width = 1080,
    height = 300,
    font_size = 110
  })
  self.createButton({
    function_owner = self,
    click_function = "toggleOrgBoard",
    position = {0, 0.25, 1.125},
    tooltip = "Enable or disable your own org board.\nThe org board can be used to place project cards in an orderly matter.\nLots of snappoints.\n Remark: Do not use the org boards of other players, may lead to bad times.",
    label = "Toggle Org Board",
    scale = {0.25,1,0.25},
    width = 1080,
    height = 300,
    font_size = 110
  })
  self.createButton({
    function_owner = self,
    click_function = "toggleIconTableau",
    position = {0.6,0.25,1.125},
    tooltip = "Toggle icon tableau.\nShows an overview of your currently played tags and your discounts.\nWorks only in conjunction with extended scripting.",
    label = "Toggle Icon Tableau",
    scale = {0.25,1,0.25},
    width = 1080,
    height = 300,
    font_size = 110
  })
  self.createButton({
    function_owner = self,
    click_function = "toggleActivationTableau",
    position = {1.2,0.25,1.125},
    tooltip = "Toggle activation tableau.\nPay your cards on the activation tableau for QoL.\nWorks only in conjunction with extended scripting.",
    label = "Toggle Activation Tableau",
    scale = {0.25,1,0.25},
    width = 1080,
    height = 300,
    font_size = 90
  })

  self.createButton({
    function_owner = self,
    click_function = "convertPlantsToGreenery",
    tooltip = "Convert 8 plants to a Greenery",
    position = {-0.90,0.25,0.865},
    scale = {0.5,1,0.5},
    color = colors.gameActionButtons.plantsToGreenery,
    width = 480,
    height = 120,
  })
  self.createButton({
    function_owner = self,
    click_function = "convertHeatToTemp",
    tooltip = "Convert 8 heat to a temperature step",
    position = {0.90,0.25,0.865},
    scale = {0.5,1,0.5},
    color = colors.gameActionButtons.heatToTemp,
    width = 480,
    height = 150,
  })
end

function toggleAntiLag()
  Global.call("toggleAntiLag", objectState.playerColor)
end

function toggleOrgBoard()
  Global.call("toggleOrg", objectState.playerColor)
end

function toggleIconTableau()
  Global.call("toggleIconTableau", objectState.playerColor)
end

function toggleActivationTableau()
  Global.call("toggleActivationTableau", objectState.playerColor)
end

function convertPlantsToGreenery()
  Global.call("plantsToGreenery", objectState.playerColor)
end

function convertHeatToTemp()
  Global.call("heatToTemp", objectState.playerColor)
end

function increaseTRPlayerClick()
  Global.call("increasePlayerTRByColor", objectState.playerColor)
end

function decreaseTRPlayerClick()
  Global.call("decreasePlayerTRByColor", objectState.playerColor)
end

function getResourceStockpile()
	local status = {}
	for name, resType in pairs(objectState.tokenGuids)  do
      if type(resType) == "table" then
			     status[name] = getObjectFromGUID(resType.stockPile).getVar("count")
      end
	end
	return status
end

function getResourceProduction()
	local status = {}
	for name, resType in pairs(objectState.tokenGuids)  do
      if type(resType) == "table" then
			     status[name] = getObjectFromGUID(resType.production).getVar("count")
      end
	end
	return status
end

function setResourceStockpile(newStatus)
		for name, type in pairs(objectState.tokenGuids) do
			if newStatus[name] ~= nil then
				getObjectFromGUID(type.stockPile).call("setCount", newStatus[name])
			end
		end
end

function setResourceProduction(newStatus)
		for name, type in pairs(objectState.tokenGuids) do
			if newStatus[name] ~= nil then
				getObjectFromGUID(type.production).call("setCount", newStatus[name])
			end
		end
end

function changeStockpileRemote(params)
  changeStockpile(params.which, params.changeAmount, params.absolute)
end

function changeStockpile(which, changeAmount, absolute)
	absolute = absolute or false
	if absolute then
		getObjectFromGUID(objectState.tokenGuids[which].stockPile).call("setCount", changeAmount)
	else
		local oldAmount = getObjectFromGUID(objectState.tokenGuids[which].stockPile).getVar("count")
		getObjectFromGUID(objectState.tokenGuids[which].stockPile).call("setCount", oldAmount + changeAmount)
	end
end

function changeProductionRemote(params)
  changeProduction(params.which, params.changeAmount, params.absolute)
end

function changeProduction(which, changeAmount, absolute)
	absolute = absolute or false
	if absolute then
		getObjectFromGUID(objectState.tokenGuids[which].production).call("setCount", changeAmount)
	else
		local oldAmount = getObjectFromGUID(objectState.tokenGuids[which].production).getVar("count")
		getObjectFromGUID(objectState.tokenGuids[which].production).call("setCount", oldAmount + changeAmount)
	end
end

function performProductionPhase(terraformingRating)
	local energyAmount = getObjectFromGUID(objectState.tokenGuids.energy.stockPile).getVar("count")
	-- Set energy to 0
	changeStockpile("energy", 0, true)
	-- Add energy to heat
	changeStockpile("heat", energyAmount, false)

	local production = getResourceProduction()

	for name, value in pairs(production) do
		-- Add production
		changeStockpile(name, value, false)
	end

	-- Add terraforming rating to credits
	changeStockpile("credits", terraformingRating)
end

function setPlayerColor(player_color)
  objectState.playerColor = player_color
  getObjectFromGUID(objectState.tokenGuids.coloredRim).setColorTint(stringColorToRGB(player_color))
end

function setPlayerName(newName)
  objectState.playerName = newName
  if not cloningOngoing then
    setDescendantPlayerNames()
  end
end

function setDescendantPlayerNames()
  for name, resType in pairs(objectState.tokenGuids) do
    if type(resType) == "table" then
      getObjectFromGUID(resType.production).setVar("playerName", objectState.playerName)
    end
  end
end
