﻿$scriptDir = Split-Path -parent $PSCommandPath
$localAppData = $env:LOCALAPPDATA

Copy-Item "$scriptDir\lua\*" -Destination "$localAppData\Temp\TableTopSimulator\Tabletop Simulator Lua\" -Recurse -Force