matrixTwoDHelpers = matrixTwoDHelpers or {}

matrixTwoDHelpers.createGrid = function(baseOffset, gridSizes, gridVectors)
  if type(gridSizes) ~= "table" or #gridSizes == 0 then
    return { baseOffset }
  end
  local subSizes = {}
  local subVectors = {}
  for i=2,#gridSizes do
      table.insert(subSizes, gridSizes[i])
      table.insert(subVectors, gridVectors[i])
  end

  local previousGrid = matrixTwoDHelpers.createGrid(baseOffset, subSizes, subVectors)
  local newGrid = {}
  for index, previousGridPoint in pairs(previousGrid) do
    for i=1,gridSizes[1] do
      newGrid[((index -1)*gridSizes[1]) + i] = vectorHelpers.addVectors(vectorHelpers.scaleVector(gridVectors[1], (i - 1)), previousGridPoint)
    end
  end
  return newGrid
end

matrixTwoDHelpers.rotateGridAroundPointY = function(point, grid, rotation)
  local result = {}
  for index,gridEntry in pairs(grid) do
      local offset = vectorHelpers.subtractVectors(gridEntry, point)
      local rotatedOffset = vectorHelpers.rotateVectorY(offset, rotation)
      result[index] = vectorHelpers.addVectors(point, rotatedOffset)
  end
  return result
end
--[[
printToAll("World Difference")
vectorHelpers.print(vectorHelpers.subtractVectors(  getObjectFromGUID("0930ac").getPosition(),  getObjectFromGUID("12612d").getPosition()))
printToAll("Local position change")
vectorHelpers.print(getObjectFromGUID("12612d").positionToLocal(getObjectFromGUID("0930ac").getPosition()))
]]

matrixTwoDHelpers.createSnapGrid = function(object, baseOffset, gridSizes, gridVectors, rotationAngle, rotationSnap, rotationVectorSnap)
  local grid = matrixTwoDHelpers.createGrid(baseOffset, gridSizes, gridVectors)
  local rotatedGrid = matrixTwoDHelpers.rotateGridAroundPointY(baseOffset, grid, rotationAngle)

  local snapInformation = {}
  for index,gridPoint in pairs(rotatedGrid) do
    snapInformation[index] = {
      position = gridPoint,
      rotation = rotationVectorSnap,
      rotation_snap = rotationSnap
    }
  end

  return snapInformation
end

matrixTwoDHelpers.scaledCoordinatesFrom1DIndexForIrregularMatrix = function(index, row_starting_offset, column_starting_offset, row_length, scaling)
  local row_index = math.floor((index - 1) / row_length) -- (index - 1) as lua indexed tables start with index 1 and we want to start with index 0 here
  local column_index = (index - 1) % row_length
  return { row_starting_offset + row_index * scaling[1], column_starting_offset + column_index * scaling[2] }
end

matrixTwoDHelpers.totalOffsetFromPositionMatrixAnd1DIndex = function(index, positionMatrix, scaling)
  local offset2D = matrixTwoDHelpers.scaledCoordinatesFrom1DIndexForIrregularMatrix(
    index, positionMatrix.rowStartingOffset, positionMatrix.columnStartingOffset, positionMatrix.rowLength, scaling or {1,1}) -- no (special) scaling applied

  return {offset2D[1], positionMatrix.heightStartingOffset, offset2D[2]}
end
