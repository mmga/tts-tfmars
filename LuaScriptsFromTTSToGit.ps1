﻿$scriptDir = Split-Path -parent $PSCommandPath
$localAppData = $env:LOCALAPPDATA

Copy-Item "$localAppData\Temp\TableTopSimulator\Tabletop Simulator Lua\*" -Destination "$scriptDir\lua\" -Recurse -Force