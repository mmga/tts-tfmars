# Changes in v7.6.1
- Various bugfixes for cards

# Changes in v7.6.0
- Added BigBox Promos and corps. Thanks to @Alexander and @Skippan
- Updated Pathfinder Mat (not cards) assets (cards will come later as they require scripting adjustments as well)
- Updated BGG Corp Assets

# Changes in v7.5.3
- Fix exception on load when Timer was not enabled
- Fix Helion payment extension not loading correctly
- Fix Pluto colony placement reward not getting put in hand

# Changes in v7.5.2
- Add place colony function to colony system. Thanks to @Rhyno
- Fix wild-cards not counting as no-tag

# Changes in v7.5.0
- Re-Added timer functionality

# Changes in v7.4.6
- Fix nil error on moving project stack
- Fix discard pile offset
- Fix cards going into project zone during drafting
- Make Fan Expansion/Randomizer toggleable

# Changes in v7.4.5
- Adjusted card buttons to new TTS update

# Changes in v7.4.4
- Fix factorum credit costs
- Fix red party bonus for solo mode
- Fix icon and activation toggle failures
- Switch small/large handzones for yellow/green players and move org boards

# Changes in v7.4.2
- Standard Project Buttons for venus and colonies
- Turmoil Scripting Phase 2
  - Turmoil Board now has buttons for each phase
  - Global Events are the only thing not implemented yet
  - Dominance is recalculated automatically after each end turn

# Changes in v7.4.1
- Following changes all thanks to @Krymnopht
- Standard project buttons for ocean, greenery and city tile (and yes, no standard project buttons for Venus and Colonies)
- Conversion buttons on player mats for 'heat -> temp' & 'plants -> greenery'
- Icon tableau reworked
  - Hideable from other players
  - New design (located above your player mat now)
  - Shows tag discounts
- Play and pay cards on your own toggleable activation tableau
  - Discounts are applied
  - Resource to credits rate is also updated if playing e.g. 'Advanced Alloys' (no support for Unity party from Turmoil)
  - Aslo introducing activation modes/rule sets (two modes: 'Restrictive' and 'Anywhere'). Can be toggled on the new activation tableau.
  - Support for helion included (you can pay with heat)

# Changes in v7.3.2
- Play chime on turn start
- Fix colony bonus

# Changes in v7.3.1
- Toggleable icon tableau for every player
- Feature: Card automation for almost all cards (except pathfinder fan expansion and turmoil global events). Big thanks to @Krymnopht
  - Enable the feature via the extended scripting toggle on the setup mat at game start
  - Tags of activated cards are tracked for each player (even without the icon tableau)
  - Production, resources, and effects are automatically applied to a player when that player activates a card
  - Tiles and card resource tokens are given to a player upon activating a card as well
  - Card resource tokens can be dropped on the respective cards and are added to the counter
- More crisp looking setup buttons. Thanks to @Xeno

Notable known issues:
- card/board explosions: cards or boards will fly in every direction and plough down anything in their way
  - Workaround: Do not use lowest lifting height, rewind time or if this doesn't help rehost the game
  - please do not repurpose other players' org boards (wrong org board orientation => card explosions)

# Changes in v7.2.13
- Fix Development center missing precondition
- Fix cards not checking resources correctly

# Changes in v7.2.12
- Fix trade functionality that got broken with 7.2.11

# Changes in v7.2.11
- Fix anti lag boards not hiding anymore
- Fix solo player colony message
- Fix off center drafting arrow
- Fix delegate initial positioning
- Change position of turmoil event board/tradeship and colonies
- Player mats now have a rim in player color again. Thanks to @Xeno
- Player mats no longer zoomable
- Changed decrease button colors
- Unofficial official save game support pre-game start

# Changes in v7.2.10
- Turn corporate era on by default
- Fix a rare issue where score counters could not be loaded if a greenery/city was deleted
- Allow delegate spawning even on rotated turmoil board
- Allows reinitializing colonies and anti lag boards after moving them. Restoring functionality.
- Anti lag and Org board no longer snap to other objects. Making placement easier
- Turmoil Initial setup
- Turmoil Cycle card function with neutral delegate placement (dominance is still manual)
- Physics complex VP counter adjusted

# Changes in v7.2.9
- Introduce button to fix player mat malrotation (might be fixed anyway now)
- Increase player mat spawning speed again

# Changes in v7.2.8
- Fix to hand zones jiggling cards around

# Changes in v7.2.7
- Fix greenery and cities not marking after oxygen is done
- Slightly moved hand zones closer to table again

# Changes in v7.2.6
- Move Venus and Turmoil Board to be closer to main area
- New Ganymede and Miranda Assets, Thanks to @Scoddard
- New Delegate Model, Thanks to @Scoddard
- Slower cloning of player material to prevent desyncs
- Allow choosing corps between 0-10. Choosing zero does build the corp deck but not shuffle.
- Changed handzones so cards should not spawn inside the table on reload
- Autoplacement of markers on greeneries and cities. Right click to prevent placement.

# Changes in v7.2.5
- Fix drafting material not spawning after finishing setup

# Changes in v7.2.4
- Fixed colony illegal double trading
- Fixed anti-lag/orgboard not working if player leaves color during setup with boards active

# Changes in v7.2.3
- Fixed rover construction initial counter
- Fixed generation end erroring out if no more delegates are available for a player

# Changes in v7.2.2
- Reign in overzealous bag protector that would not allow delegates to enter bag on a savegame reload
- Antilag board now hides cards correctly on reload. And does not hide the table.

# Changes in v7.2.1
- Game does not end anymore if global parameters were filled during solar phase

# Changes in v7.2.0

- Solo rules for colonies, Venus win condition and generation limit implemented
- Solo initial 4 cards drawn
- Card fixes for immigration city, Olympus conference, orbital cleanup
- Variant Mat for options that are not official gameplay rules but variants
- Message on maxing out global parameters
- Fix for non working temperature/oxygen markers (final hopefully)
- Fix ocean plate recreation on delete
- Hide Turn System on Game Ending
- Move research button to center of playing field after setup finished
- Fixed red player trade fleet scaling
- Fix error on generation end if drafting is off
- Faster spawning of material
- Moved score counters up so they don't overlap with anti lag board
