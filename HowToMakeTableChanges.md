If you want to change something on the table or on existing objects do the following:

- Fork the repository (only needed once) or create a new branch
- Check with issues to see if someone is already working on it
- Create an issue that describes what you want to do or get feedback on your idea (if it is more than a minor change)
- Copy the reference save file to your TTS Save folder
- Make your changes to the reference save and overwrite the file in TTS
- Copy the file back overwriting the reference file
- ONLY commit the changes that are necessary, omitting the scripts for the object and any other random changes that TTS made to the save
- Test your changes by building a new save file (see also [Building.md](./Building.md))
- Create a merge request from your branch/fork to master detailing what you've done
