If you want to change something in the scripts, then the steps are as follows:

- Fork the repository (only needed once) or create a new branch
- Check with issues to see if someone is already working on it
- Create an issue that describes what you want to do or get feedback on your idea (if it is more than a minor change)
- Make your changes to the code
- Test your changes to the code by embedding them into a save file (see also [Building.md](./Building.md))
- Create a merge request from your branch/fork to master detailing what you've done

FAQ:

### I want to create a script for an existing object that doesn't have a script yet!

- Create a scriptfile named "ObjectName.ObjectGUID.ttslua" in the lua folder, replacing ObjectName and ObjectGUID with the values in game
- Embed the script into the savefile using the TTSLuaExtractor (check [Building.md](./Building.md))

### I want to create a new object with a script!

- Check [HowToMakeTableChanges](./HowToMakeTableChanges) to make the changes to the table and then put the object script as above
